Projet de transformation d'un fichier XML (avec XQuery)
Par Thibaut Godet et Mathis Faivre

Le fichier XML source est à mettre à la racine du dossier, il doit être nommé :
"AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml"
Utilise eXist-db (version 4.5.0)

---
Pour exécuter et comparer le programme XQuery :
Exécuter le programme bash 'executer-xquery.sh' avec un terminal bash, par exemple : 
$ ./executer-xquery.sh

Si le programme bash ne s'execute pas parce que vous n'avez pas les droits, exécuter :
$ chmod +x executer-tout.sh
