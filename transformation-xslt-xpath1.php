<?php
// Le processeur XSLT inclus dans PHP est seulement compatible avec XPath 1.0
header('Content-type: text/xml; Encoding: utf-8');
$xml_doc = new DOMDocument();
$xml_doc->load("AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml");
$xsl_doc = new DOMDocument();
$xsl_doc->load("transformation-xslt-xpath1.xsl");
$proc = new XSLTProcessor();
$proc->importStylesheet($xsl_doc);
echo $proc->transformToXml($xml_doc);