<?php 
header('Content-type: text/xml; Encoding: utf-8');
include('Sax4PHP.php');

class transformationSax extends DefaultHandler {
    //Déclaration variables
    private $multipleTexts;
    private $texte, $ok, $age;
    private $isActor = false, $isMandat = false, $isPresident = false, $isOrgane = false, $isCollaborateur = false, $isTrueMandat = false;
    private $prenom, $nom, $organeRef, $dateDebut, $dateFin, $datePub, $legislature, $uidOrgane, $uidActor, $uid;
    private $libelles = array(), $personnes = array(), $mandats = array(), $personneMandats = array();
	
    //On affiche l'en-tête et on ouvre la balise racine nantais
	function startDocument() {
    	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo "<!DOCTYPE nantais SYSTEM \"ex.dtd\">\n";
		echo "<nantais>\n";
	}
	
    //On affiche toutes les données à la fin du traitement
	function endDocument() {
        //On trie les personnes par leur nom
        ksort($this->personnes);
        foreach($this->personnes as $nom => $personne){
            //On affiche la balise personne avec les attributs
            echo $personne;
            //Pour chaque personne on récupère tous leurs mandats
            foreach($this->personneMandats as $nom2 => $mandats){
                if($nom == $nom2){
                    //On trie les mandats par la date de début
                    ksort($mandats);
                    //On affiche tous les mandats de chaque personne
                    foreach($mandats as $mandat){
                        //On affiche la balise md avec les différents attributs
                        echo $mandat;
                    }  
                }
            }
            //On ferme la balise personne
            echo "\t</personne>\n";
        }
        //Fin du document on ferme la balise nantais
		echo "</nantais>\n";
	}

  	function characters($txt) {
		if ($this->multipleTexts) {
			$this->texte .= $txt;
		} else {
			$this->multipleTexts = true;
			$this->texte = $txt;
        }
  	}
  
	function startElement($nom, $att) {
        switch($nom) {
            //Si on entre dans une balise spéciale on met les flags nécessaires à vrai
            //pour le traitement dans la fonction endElement
            case 'acteur' : 
                $this->isActor = true;
                $this->isCollaborateur = false;
                break;
            case 'mandat' :
                $this->isMandat = true;
                $this->legislature  = "";
                $this->organeRef = "";
                $this->dateDebut = "";
                $this->dateFin = "";
                $this->datePub = "";
                break;
            case 'organe' :
                $this->isOrgane = true;
                break;
            case 'collaborateur' :
                $this->isCollaborateur = true;
            default :;
        }
	}
	  
	function endElement($nom) {
		if ($this->multipleTexts) {
			$this->multipleTexts = false;
		}
		$this->texte = trim($this->texte);
        switch($nom) {
            case 'uid' :
                if($this->isOrgane){
                    $this->uidOrgane = $this->texte;
                }
                if($this->isMandat){
                    $this->uid = $this->texte;
                }
                break;
            case 'libelle' :
                if($this->isOrgane){
                    $this->libelles[$this->uidOrgane] = $this->texte;
                }
                break;
            case 'prenom' :
                if(($this->isActor) && !($this->isCollaborateur)){
                    $this->prenom = $this->texte;
                }
                break;
            case 'nom' :
                if(($this->isActor) && !($this->isCollaborateur)){
                    $this->nom = $this->texte;
                }
                break;
            case 'acteur' :
                $this->isActor = false;
                $this->isNantais = false;
                $this->isPresident = false;
                $this->personneMandats[$this->nom] = $this->mandats;
                $this->mandats = array();
                break;
            case 'villeNais' :
                $this->isNantais = $this->texte == "Nantes";
                break;
            case 'organeRef' :
                if($this->isMandat){
                    $this->organeRef = $this->texte;
                }
                break;
            case 'dateDebut' :
                if($this->isMandat){
                    $this->dateDebut = $this->texte;
                }
                break;
            case 'dateFin' :
                if($this->isMandat){
                    $this->dateFin = $this->texte;
                }
                break;
            case 'datePublication' :
                if($this->isMandat){
                    $this->datePub = $this->texte;
                }
                break;
            case 'legislature' :
                if($this->isMandat){
                    $this->legislature = $this->texte;
                }
                break;
            case 'codeQualite' :
                if($this->isMandat && $this->isNantais){
                    if($this->texte == "Président"){
                        $this->isPresident = true;
                        $this->isTrueMandat = true;
                    }
                }
                break;
            case 'mandat' :
                if($this->isTrueMandat && $this->isPresident && $this->isNantais && $this->isTrueMandat){
                    if($this->datePub != ""){
                        $this->datePub = " pub=\"".$this->datePub . "\"";
                    }
                    if($this->dateFin != ""){
                        $this->dateFin = " fin=\"".$this->dateFin . "\"";
                    }
                    //On stock les balise mandats d'une personne dans un tableau associatif DateDeb + uid => Balise Mandats
                    $this->mandats[$this->dateDebut . $this->uid] = "\t\t<md code=\"".$this->organeRef."\" début=\"".$this->dateDebut."\" legislature=\"".$this->legislature."\"". $this->datePub . $this->dateFin.">".$this->libelles[$this->organeRef]."</md>\n";
                } 
                $this->isMandat = false;
                $this->isTrueMandat = false;
                break;
            case 'mandats' :
                //On stock la balise personne dans un tableau associatif Nom => Balise Personne
                if($this->isNantais && $this->isPresident){
                    $this->personnes[$this->nom] = "\t<personne nom=\"" . $this->prenom." ".$this->nom."\">\n";
                }
                break;
            case 'collaborateur' :
                $this->isCollaborateur = false;
            default :;
        }
	}
}

try {
	$sax = new SaxParser(new transformationSax());
	$sax->parseFileLineByLine('AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml');
} catch(SAXException $e){
	echo "\n",$e;
} catch(Exception $e) {
	echo "Capture de l'exception par défaut\n", $e;
}