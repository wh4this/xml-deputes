xquery version "3.1" encoding "utf-8";
<nantais> {
    for $acteur in doc("AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml")/export/acteurs/acteur
    where $acteur/etatCivil/infoNaissance/villeNais eq "Nantes" and $acteur/mandats/mandat/infosQualite/codeQualite = 'Président'
    let $prenomnom := (concat($acteur/etatCivil/ident/prenom, ' ', $acteur/etatCivil/ident/nom))
    let $nomprenom := (concat($acteur/etatCivil/ident/nom, ' ', $acteur/etatCivil/ident/prenom))
    order by $nomprenom
    return
        <personne nom="{ $prenomnom }"> {
            for $mandat in $acteur/mandats/mandat
            where $mandat/infosQualite/codeQualite eq "Président"
            order by $mandat/dateDebut
            return element md  { 
                attribute code { $mandat/organes/organeRef },
                
                attribute début { $mandat/dateDebut },
                
                attribute legislature { $mandat/legislature },
                
                if ($mandat/dateFin ne '') then 
                    attribute fin { $mandat/dateFin } 
                else (),
                
                if ($mandat/datePublication ne '') then 
                    attribute pub { $mandat/datePublication } 
                else (),
                
                doc("AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml")/export/organes/organe[./uid eq $mandat/organes/organeRef]/libelle/text()
            }
        } </personne>
} </nantais>