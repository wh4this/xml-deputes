#!/bin/bash
# http://exist-db.org/exist/apps/doc/java-admin-client.xml#command-line 

# Cree le dossier de sortie (si il n'existe pas deja)
mkdir -p sortie;

echo "Appuyez sur Entrer si la valeur entre parenthèse est la bonne, 
la valeur en majuscule est priorisée quand il y a un choix."
echo
# Demande le chemin pour executer client.sh
read -p "Entrez le chemin absolu vers le dossier de votre client eXist-db 
(/Applications/eXist-db.app/Contents/Resources/eXist-db/bin/) : " chemin_vers_existdb_client
if [ -z "$chemin_vers_existdb_client" ];  then
    chemin_vers_existdb_client="/Applications/eXist-db.app/Contents/Resources/eXist-db/bin/"
fi

read -p "Exécuter localement (o/N)? " choix
case "$choix" in 
  o|O ) execution_locale=true;;
  n|N ) execution_locale=false;;
  * ) execution_locale=false;;
esac

read -p "Entrez le nom d'utilisateur eXist-db (admin) : " nom_utilisateur;
if [ -z "$nom_utilisateur" ];  then
    nom_utilisateur="admin"
fi

read -p "Entrez le mot de passe eXist-db () : " mot_de_passe;
if [ -z "$mot_de_passe" ];  then
    mot_de_passe=""
fi

# On sauvegarde le chemin vers le dossier courant
dossier_courant=$(pwd);

# Se rend de le dossier du client exist-db
cd $chemin_vers_existdb_client;

read -p "Importer le fichier XML (O/n)? " import
case "$import" in 
  o|O ) import=true;;
  n|N ) import=false;;
  * ) import=true;;
esac

# Importe le fichier
# https://stackoverflow.com/a/21210966
if [ "$import" = true ]; then
    echo "Import en cours..."
    if [ -z "$mot_de_passe" ];  then
        if [ "$execution_locale" = true ]; then
            ( . client.sh -s -l -u $nom_utilisateur -m /db/ -p "$dossier_courant/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml" ) > /dev/null
        else
            ( . client.sh -s -u $nom_utilisateur -m /db/ -p "$dossier_courant/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml" ) > /dev/null
        fi
    else
        if [ "$execution_locale" = true ]; then
            ( . client.sh -s -l -u $nom_utilisateur -P $mot_de_passe -m /db/ -p "$dossier_courant/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml" ) > /dev/null
        else
            ( . client.sh -s -u $nom_utilisateur -P $mot_de_passe -m /db/ -p "$dossier_courant/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.xml" ) > /dev/null
        fi
    fi  
fi

echo "Exécution en cours..."

# Execute la requête xquery
if [ -z "$mot_de_passe" ];  then
    if [ "$execution_locale" = true ]; then
        ( . client.sh -s -l -u $nom_utilisateur -F "$dossier_courant/transformation-xquery.xq" ) > $dossier_courant/sortie/resultat-xquery.xml
    else
        ( . client.sh -s -u $nom_utilisateur -F "$dossier_courant/transformation-xquery.xq") > $dossier_courant/sortie/resultat-xquery.xml
    fi
else
    if [ "$execution_locale" = true ]; then
        ( . client.sh -s -l -u $nom_utilisateur -P $mot_de_passe -F "$dossier_courant/transformation-xquery.xq" ) > $dossier_courant/sortie/resultat-xquery.xml
    else
        ( . client.sh -s -u $nom_utilisateur -P $mot_de_passe -F "$dossier_courant/transformation-xquery.xq") > $dossier_courant/sortie/resultat-xquery.xml
    fi
fi  


cd $dossier_courant

# On supprime les warnings et les messages de connexion au serveur eXist-db dans le fichier resultat
# Nous n'avons pas trouver d'autre moyen pour mettre le résultat dans un fichier (--output ne fonctionne pas avec -F)
sed -i '' -e '1,/^Connected :-)$/d' sortie/resultat-xquery.xml
if [ "$execution_locale" = true ]; then
    sed -i '' -e '/shutting down database.../d' ./sortie/resultat-xquery.xml
fi


echo
echo "Le résultat se trouve dans le dossier \"sortie\"."
